﻿using Microsoft.AspNetCore.Mvc;
using ProductService.Domain.Shared.Contracts;
using ProductService.Domain.Shared.Model.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductManager _productManager;
        public ProductController(IProductManager productManager)
        {

            _productManager = productManager;
        }

        [HttpPost("Add")]

        public async Task<ActionResult<bool>> Add([FromBody] AddProduct product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return await _productManager.AddProduct(product);
        }

        [HttpDelete("{Id}")]

        public async Task<ActionResult<bool>> Delete(string Id)
        {
            if (String.IsNullOrEmpty(Id))
            {
                return BadRequest("Invalid product Id");
            }
            Guid productId = Guid.Parse(Id);

            return await _productManager.DeleteProduct(productId);

        }

        [HttpGet]
        public async Task<ActionResult<List<AddProduct>>> GetAllProducts()
        {
            var products = await _productManager.GetProducts();
            return products.Select(p => new AddProduct
            {
                CategoryId = p.CategoryId,
                Price = p.Price,
                Description = p.Description,
                IsActive = p.IsActive,
                Id = p.Id,
                Quantity = p.Quantity,
                Rating = p.Rating,
                Name=p.Name
            }).ToList();

        }

        [HttpPost("Update")]
        public async Task<ActionResult<bool>> Update(AddProduct product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid product for update");
            }
            return await _productManager.Update(product);

        }

    }
}
