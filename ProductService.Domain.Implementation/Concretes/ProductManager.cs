﻿using AutoMapper;
using ProductService.Domain.Shared.Contracts;
using ProductService.Domain.Shared.Model.Request;
using ProductService.Repository.Shared.Contracts;
using ProductService.Repository.Shared.Enities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductService.Domain.Implementation.Contracts
{
    public class ProductManager : IProductManager
    {
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;

        public ProductManager(IProductRepository productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper = mapper;
        }
        public async Task<bool> AddProduct(AddProduct Product)
        {
            var prod = _mapper.Map<Product>(Product);
            return await _productRepository.Add(prod);
        }

        public async Task<bool> DeleteProduct(Guid Id)
        {
            return await _productRepository.Delete(Id);
        }

        public async Task<List<Product>> GetProducts()
        {
            return await _productRepository.GetProducts();
        }

        public async  Task<bool> Update(AddProduct Product)
        {
            var prod = _mapper.Map<Product>(Product);
            return await _productRepository.Update(prod);
        }
    }
}
