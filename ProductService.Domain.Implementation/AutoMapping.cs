﻿using AutoMapper;
using ProductService.Domain.Shared.Model.Request;
using ProductService.Repository.Shared.Enities;

namespace ProductService.Domain.Implementation
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<AddProduct, Product>();
        }
    }
}
