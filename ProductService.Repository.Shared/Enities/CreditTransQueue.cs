﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace ProductService.Repository.Shared.Enities
{
    [Keyless]
    [Table("CreditTransQueue")]
    public partial class CreditTransQueue
    {
        [Column("QueueID")]
        public long QueueId { get; set; }
        [Required]
        [StringLength(50)]
        public string ProductType { get; set; }
        [Required]
        [StringLength(50)]
        public string ProductSrNo { get; set; }
        [Required]
        [StringLength(50)]
        public string TransType { get; set; }
        [Column("JobID")]
        [StringLength(500)]
        public string JobId { get; set; }
        [StringLength(1000)]
        public string JobNo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Date { get; set; }
        public int? Status { get; set; }
        [Column("ContractID")]
        public int ContractId { get; set; }
        [MaxLength(1000)]
        public byte[] Token { get; set; }
        [Column("CompanyID")]
        public int? CompanyId { get; set; }
        [Column("BranchID")]
        public int? BranchId { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        [StringLength(1000)]
        public string Remark { get; set; }
    }
}
