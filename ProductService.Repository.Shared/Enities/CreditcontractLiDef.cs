﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace ProductService.Repository.Shared.Enities
{
    [Keyless]
    [Table("creditcontract_LI_Def")]
    public partial class CreditcontractLiDef
    {
        [Required]
        [StringLength(50)]
        public string ProductType { get; set; }
        [Required]
        [StringLength(50)]
        public string ProductSrNo { get; set; }
        [Column(TypeName = "text")]
        public string ContractData { get; set; }
        public int? ProdStatus { get; set; }
        [Column("ContractID")]
        public int? ContractId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedOn { get; set; }
        [MaxLength(1000)]
        public byte[] CreditToken { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? BalanceUpdatedOn { get; set; }
    }
}
