﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace ProductService.Repository.Shared.Enities
{
    public partial class SysSetting
    {
        [Key]
        [Column("SettingID")]
        [StringLength(50)]
        public string SettingId { get; set; }
        [StringLength(50)]
        public string SettingValue { get; set; }
    }
}
