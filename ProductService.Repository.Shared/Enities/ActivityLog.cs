﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace ProductService.Repository.Shared.Enities
{
    [Table("ActivityLog")]
    public partial class ActivityLog
    {
        [Key]
        [Column("LogID")]
        public long LogId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LogDate { get; set; }
        [StringLength(50)]
        public string LogType { get; set; }
        public string LogText { get; set; }
    }
}
