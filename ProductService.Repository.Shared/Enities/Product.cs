﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace ProductService.Repository.Shared.Enities
{
    [Table("Product")]
    public partial class Product
    {
        [Key]
        public Guid Id { get; set; }
        public int CategoryId { get; set; }
        public double Price { get; set; }
        [Column(TypeName = "text")]
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int Quantity { get; set; }
        public double Rating { get; set; }
        [StringLength(100)]
        public string Name { get; set; }

        [ForeignKey(nameof(CategoryId))]
        [InverseProperty("Products")]
        public virtual Category Category { get; set; }
    }
}
