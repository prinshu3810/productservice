﻿using ProductService.Repository.Shared.Enities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductService.Repository.Shared.Contracts
{
    public interface IProductRepository
    {
        Task<bool> Add(Product Product);
        Task<bool> Delete(Guid Id);
        Task<List<Product>> GetProducts();
        Task<bool> Update(Product Product);
    }
}
