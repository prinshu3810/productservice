﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ProductService.Repository.Shared.Enities
{
    public partial class CreditQueueSystemContext : DbContext
    {
        public CreditQueueSystemContext()
        {
        }

        public CreditQueueSystemContext(DbContextOptions<CreditQueueSystemContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ActivityLog> ActivityLogs { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CreditContract> CreditContracts { get; set; }
        public virtual DbSet<CreditTransQueue> CreditTransQueues { get; set; }
        public virtual DbSet<CreditcontractLiDef> CreditcontractLiDefs { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<SysSetting> SysSettings { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<ActivityLog>(entity =>
            {
                entity.Property(e => e.LogType).IsUnicode(false);
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<CreditContract>(entity =>
            {
                entity.HasKey(e => new { e.ProductType, e.ProductSrNo })
                    .HasName("PK__CreditContract");

                entity.Property(e => e.ProductType).IsUnicode(false);

                entity.Property(e => e.ProductSrNo).IsUnicode(false);
            });

            modelBuilder.Entity<CreditTransQueue>(entity =>
            {
                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.JobId).IsUnicode(false);

                entity.Property(e => e.JobNo).IsUnicode(false);

                entity.Property(e => e.ProductSrNo).IsUnicode(false);

                entity.Property(e => e.ProductType).IsUnicode(false);

                entity.Property(e => e.QueueId).ValueGeneratedOnAdd();

                entity.Property(e => e.Remark).IsUnicode(false);

                entity.Property(e => e.TransType).IsUnicode(false);
            });

            modelBuilder.Entity<CreditcontractLiDef>(entity =>
            {
                entity.Property(e => e.ProductSrNo).IsUnicode(false);

                entity.Property(e => e.ProductType).IsUnicode(false);
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Name).IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_product_category");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
