﻿using ProductService.Domain.Shared.Model.Request;
using ProductService.Repository.Shared.Enities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductService.Domain.Shared.Contracts
{
    public interface IProductManager
    {
        Task<bool> AddProduct(AddProduct Product);
        Task<bool> DeleteProduct(Guid Id);
        Task<List<Product>> GetProducts();
        Task<bool> Update(AddProduct Product);

    }
}
