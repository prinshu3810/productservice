﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProductService.Domain.Shared.Model.Request
{
    public class AddProduct
    {
        public Guid Id { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public double Price { get; set; }
        public string Description { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public double Rating { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
