﻿using Microsoft.EntityFrameworkCore;
using ProductService.Repository.Shared.Contracts;
using ProductService.Repository.Shared.Enities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductService.Repository.Implementation
{

    public class ProductRepository : IProductRepository
    {
        private CreditQueueSystemContext _dbContext;
        public ProductRepository(CreditQueueSystemContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<bool> Add(Product product)
        {

            try
            {
                _dbContext.Entry(product).State = EntityState.Added;
                _dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }

            return true;

        }

        public async Task<bool> Delete(Guid Id)
        {

            try
            {
                var product = _dbContext.Products.FirstOrDefault(p => p.Id == Id);
                _dbContext.Products.Remove(product);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<List<Product>> GetProducts()
        {
            return await _dbContext.Products.ToListAsync();
        }

        public async Task<bool> Update(Product Product)
        {
            try
            {
                _dbContext.Entry(Product).State = EntityState.Modified;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }
    }
}
